import csv
import json
import os
import pathlib
import pickle
import re
import sys


class AbstractReader:
    ALLOWED_EXTENSION = (
        "json",
        "csv",
        "pickle"
    )

    def __init__(self, file_path, list_of_changes, dst_file_path):
        self.file_path = file_path
        self.list_of_changes = list_of_changes
        self.dst_file = dst_file_path
        self.file_type = self.check_filetype()
        self.validate()
        self.validate_file_path()
        self.data = self.load_data()

    def check_filetype(self):
        return pathlib.Path(self.file_path).suffix[1:]

    def load_data(self):
        raise NotImplementedError

    def modify_file(self):
        changes = self.transform_list_of_changes()
        for elem in changes:
            row, col, change_value = elem
            self.data[int(row)][int(col)] = change_value

    def save_file(self):
        if self.dst_file.endswith("csv"):
            self.save_to_csv()
        elif self.dst_file.endswith("json"):
            self.save_to_json()
        elif self.dst_file.endswith(".pickle"):
            self.save_to_pickle()

        else:
            raise ValueError("Wrong dst file format.")

    def transform(self):
        self.modify_file()
        self.save_file()

    def validate(self):
        if self.file_type not in AbstractReader.ALLOWED_EXTENSION:
            raise ValueError(f"Plik w niedozwolonym formacie: {self.file_type}")

    def transform_list_of_changes(self):
        lista_zmian = []
        for i in range(len(self.list_of_changes)):
            wartosc_zmieniona_jako_lista = self.list_of_changes[i].split(',')
            lista_zmian.append(wartosc_zmieniona_jako_lista)

        return lista_zmian

    def validate_file_path(self):
        if not os.path.isfile(self.file_path):
            dir_path = os.path.dirname(self.file_path)
            if dir_path == '':
                dir_path = os.getcwd()
            msg = f"Podany plik nie istnieje. Lista dostepnych plików w katalogu to {os.listdir(dir_path)}"
            raise FileNotFoundError(msg)

    def save_to_csv(self):
        with open(self.dst_file, 'w') as dst_file:
            writer = csv.writer(dst_file)
            for row in self.data:
                writer.writerow(row)

    def save_to_pickle(self):
        with open(self.dst_file, 'wb') as pickle_f:
            pickle.dump(self.data, pickle_f)

    def save_to_json(self):
        result = {}
        for i, line in enumerate(self.data):
            s = iter(self.data[i][1:])
            result[line[0]] = dict(zip(s, s))

        with open(self.dst_file, "w") as fp:
            json.dump(result, fp)


class JsonReader(AbstractReader):
    def __init__(self, file_path, list_of_changes, dst_file_path):
        super().__init__(file_path, list_of_changes, dst_file_path)

    def load_data(self):
        lines = []
        with open(self.file_path) as f:
            for line in f:
                row = [row.replace("{", "").replace("}", "").replace('"', "").strip() for row in re.split(r':|,', line)]
                new_row = list(filter(None, row))
                lines.append(new_row)
        return lines


class CsvReader(AbstractReader):
    def __init__(self, file_path, list_of_changes, dst_file_path):
        super().__init__(file_path, list_of_changes, dst_file_path)

    def load_data(self):
        read_rows = []
        with open(self.file_path) as src_file:
            lines = csv.reader(src_file)
            for row in lines:
                read_rows.append(row)

        return read_rows


class PickleReader(AbstractReader):
    def __init__(self, file_path, list_of_changes, dst_file_path):
        super().__init__(file_path, list_of_changes, dst_file_path)

    def load_data(self):
        with open(self.file_path, "rb") as p:
            return pickle.load(p)


def main():
    arguments = sys.argv[1:]
    src = arguments[0]
    dst = arguments[1]
    changes = arguments[2:]

    if src.endswith("json"):
        csv_reader = JsonReader(src, list_of_changes=changes, dst_file_path=dst)
        csv_reader.transform()

    elif src.endswith(".csv"):
        csv_reader = CsvReader(src, list_of_changes=changes, dst_file_path=dst)
        csv_reader.transform()

    elif src.endswith('.pickle'):
        pickle_reader = PickleReader(src, list_of_changes=changes, dst_file_path=dst)
        pickle_reader.transform()

    else:
        raise ValueError("Nieznany format.")


if __name__ == '__main__':
    """Przykładowe wywołanie: python reader_2.py file.json new.json '1,0,"hello"'"""
    main()
